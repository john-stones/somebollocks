actionCostMap = {
    one_action: "[one-action]",
    two_actions: "[two-actions]",
    three_actions: "[three-actions]",
    reaction: "[reaction]",
    free_action: "[free-action]",
}

function createActionElement(action) {
    let actionElement = document.createElement("div");
    actionElement.classList.add("action");

    let actionNameCostElement = document.createElement("div");
    actionNameCostElement.classList.add("action-name-cost");
    actionElement.appendChild(actionNameCostElement);

    let actionNameElement = document.createElement("div");
    actionNameElement.classList.add("action-name");
    actionNameElement.textContent = action.name;
    actionNameCostElement.appendChild(actionNameElement);

    for (let cost of action.cost) {
        let actionCostElement = document.createElement("span");
        actionCostElement.classList.add("action-cost");
        actionCostElement.textContent = actionCostMap[cost];
        actionNameCostElement.appendChild(actionCostElement);
    }

    let actionDescriptionElement = document.createElement("div");
    actionDescriptionElement.classList.add("action-short-description");
    actionDescriptionElement.textContent = action.shortDescription;
    actionElement.appendChild(actionDescriptionElement);

    let actionPageElement = document.createElement("div");
    actionPageElement.classList.add("action-page");
    actionPageElement.textContent = action.page;
    actionElement.appendChild(actionPageElement);

    let actionTagsElement = document.createElement("ul");
    actionTagsElement.classList.add("action-tags");
    for (let actionTag of action.tags) {
        let actionTagElement = document.createElement("span");
        actionTagElement.classList.add("action-tag");
        actionTagElement.textContent = actionTag;
        actionTagsElement.appendChild(document.createElement("li"))
            .appendChild(actionTagElement);
    }
    for (let actionCustomTag of action.customTags) {
        let actionTagElement = document.createElement("span");
        actionTagElement.classList.add("action-tag-custom");
        actionTagElement.textContent = actionCustomTag;
        actionTagsElement.appendChild(document.createElement("li"))
            .appendChild(actionTagElement);
    }
    if (actionTagsElement.children.length === 0) {
        actionTagsElement.classList.add("hidden");
    }
    actionElement.appendChild(actionTagsElement);

    let actionExtra = document.createElement("div");
    actionExtra.classList.add("action-extra");
    actionExtra.classList.add("hidden");
    actionElement.appendChild(actionExtra);

    let actionLongDescription = document.createElement("div");
    actionLongDescription.classList.add("action-description");
    actionLongDescription.textContent = action.description;
    actionExtra.appendChild(actionLongDescription);

    let actionProcedure = document.createElement("div");
    actionProcedure.classList.add("action-procedure");
    actionProcedure.textContent = action.procedure;
    actionExtra.appendChild(actionProcedure);

    actionElement.onclick = function () {
        actionExtra.classList.toggle("hidden");
    }

    return actionElement;
}

// Add encounter actions
let encounterActionsElement = document.querySelector("#list-actions-encounter");

function setChildren(target, children) {
    target.innerHTML = "";
    children.forEach(child => target.appendChild(child));
    return target;
}

function updateActions(filter, comparator) {
    let filteredActionItemElements = actions
        .filter(filter)
        .sort(comparator)
        .map(createActionElement)
        .map(actionElement => {
            let itemElement = document.createElement("li");
            itemElement.appendChild(actionElement);
            return itemElement;
        });

    setChildren(encounterActionsElement, filteredActionItemElements);
}

const defaultFilter = _ => true;
const defaultComparator =
    (action1, action2) => action1.name.localeCompare(action2.name);

let currentFilter = defaultFilter;
let currentComparator = defaultComparator;

updateActions(currentFilter, currentComparator);

// Setup encounter action filter
function parseActionFilter(filterString) {
    let filterStringLower = filterString.toLowerCase();

    return action => {
        let actionContainsTag =
            action.tags
                .map(s => s.toLowerCase())
                .includes(filterStringLower)
            || action.customTags
                .map(s => s.toLowerCase())
                .includes(filterStringLower);

        let actionNameContains = action.name.toLowerCase().includes(filterStringLower);

        return !filterString || actionContainsTag || actionNameContains;
    };
}

function filterActionElements() {
    let filterQueryElement =
        document.querySelector("#actions-encounter-filter .filter-query");

    currentFilter = parseActionFilter(filterQueryElement.value);
    filterQueryElement.value = "";

    updateActions(currentFilter, currentComparator);
}

document.querySelector("#actions-encounter-filter .filter-submit")
    .onclick = filterActionElements();

document.querySelector("#actions-encounter-filter .filter-query")
    .addEventListener("keydown", event => {
        if (event.code === "Enter") {
            filterActionElements();
        }
    });

// Encounter action sorting
document.querySelector("#button-sort-name").onclick = () => {
    currentComparator =
        (action1, action2) => action1.name.localeCompare(action2.name);

    updateActions(currentFilter, currentComparator);
};

document.querySelector("#button-sort-page").onclick = () => {
    currentComparator =
        (action1, action2) => action1.page - action2.page;

    updateActions(currentFilter, currentComparator);
};

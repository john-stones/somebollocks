actions = [{
    "name": "Aid",
    "shortDescription": "Add bonus to a creature's skill check",
    "description": `*Trigger* An ally is about to use an action that requires a skill check or attack roll.

*Requirements* The ally is willing to accept your aid, and you have prepared to help (see below).

---

You try to help your ally with a task. To use this reaction, you must first prepare to help, usually by using an action during your turn. You must explain to the GM exactly how you’re trying to help, and they determine whether you can Aid your ally.

When you use your Aid reaction, attempt a skill check or attack roll of a type decided by the GM. The typical DC is 20, but the GM might adjust this DC for particularly hard or easy tasks. The GM can add any relevant traits to your preparatory action or to your Aid reaction depending on the situation, or even allow you to Aid checks other than skill checks and attack rolls.`,
    "procedure": `*Roll* Skill or Attack

*DC* 20 [+ difficulty]

---

*Critical Success* Give ally +2 circumstance bonus to the triggering check (master: +3, legendary: +4).

*Success* Give ally +1 circumstance bonus to the triggering check.

*Critical Failure* Give ally –1 circumstance penalty to the triggering check.`,
    "cost": ["reaction"],
    "tags": [],
    "customTags": ["bonus", "ally", "skill"],
    "page": 470,
},
    {
        "name": "Crawl",
        "shortDescription": "Move 5 feet while Prone",
        "description": `*Requirements* You are prone and your Speed is at least 10 feet.

---

You move 5 feet by crawling and continue to stay prone.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["prone"],
        "page": 470,
    },
    {
        "name": "Delay",
        "shortDescription": "Delay action",
        "description": `*Trigger* Your turn begins.

---

You wait for the right moment to act. The rest of your turn doesn’t happen yet. Instead, you’re removed from the initiative order. You can return to the initiative order as a free action triggered by the end of any other creature’s turn. This permanently changes your initiative to the new position. You can’t use reactions until you return to the initiative order. If you Delay an entire round without returning to the initiative order, the actions from the Delayed turn are lost, your initiative doesn’t change, and your next turn occurs at your original position in the initiative order.

When you Delay, any persistent damage or other negative effects that normally occur at the start or end of your turn occur immediately when you use the Delay action. Any beneficial effects that would end at any point during your turn also end. The GM might determine that other effects end when you Delay as well. Essentially, you can’t Delay to avoid negative consequences that would happen on your turn or to extend beneficial effects that would end on your turn.`,
        "procedure": `Become Delayed (can only use the Stop (Delay) action)

End your turn (p469)`,
        "cost": ["free_action"],
        "tags": [],
        "customTags": [],
        "page": 470,
    },
    {
        "name": "Stop (Delay)",
        "shortDescription": "Act while Delayed",
        "description": `*Requirements* You are Delayed.

*Trigger* End of another creature's turn.

---

See Delay.`,
        "procedure": `Remove Delayed

Move initiative immediately after the triggering creature

Act (p469)

Skip turn`,
        "cost": ["free_action"],
        "tags": [],
        "customTags": [],
        "page": 470,
    },
    {
        "name": "Drop Prone",
        "shortDescription": "Become Prone",
        "description": `You fall prone.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["prone"],
        "page": 470,
    },
    {
        "name": "Escape",
        "shortDescription": "Remove Grabbed, Restrained, or Immobilized",
        "description": `You attempt to escape from being grabbed, immobilized, or restrained. Choose one creature, object, spell effect, hazard, or other impediment imposing any of those conditions on you. Attempt a check using your unarmed attack modifier against the DC of the effect. This is typically the Athletics DC of a creature grabbing you, the Thievery DC of a creature who tied you up, the spell DC for a spell effect, or the listed Escape DC of an object, hazard, or other impediment. You can attempt an Acrobatics or Athletics check instead of using your attack modifier if you choose (but this action still has the attack trait).`,
        "procedure": `*Target* 1 creature

*Roll* Unarmed Attack, Acrobatics, or Athletics

*DC* Escape, Athletics, Thievery, or Spell

---

*Critical Success* As success and you can Stride up to 5 feet.

*Success* Remove Grabbed, Immobilized, and Restrained imposed by the target.

*Critical Failure* You can’t Escape until your next turn.`,
        "cost": ["one_action"],
        "tags": ["attack"],
        "customTags": ["unarmed", "acrobatics", "athletics", "grabbed", "immobolized", "restrained"],
        "page": 470,
    },
    {
        "name": "Interact",
        "shortDescription": "Use an object",
        "description": `You use your hand or hands to manipulate an object or the terrain. You can grab an unattended or stored object, open a door, or produce some similar effect. You might have to attempt a skill check to determine if your Interact action was successful.`,
        "cost": ["one_action"],
        "tags": ["manipulate"],
        "customTags": [],
        "page": 470,
    },
    {
        "name": "Leap",
        "shortDescription": "Jump over spaces",
        "description": `You take a careful, short jump. You can Leap up to 10 feet horizontally if your Speed is at least 15 feet, or up to 15 feet horizontally if your Speed is at least 30 feet. You land in the space where your Leap ends (meaning you can typically clear a 5-foot gap, or a 10-foot gap if your Speed is 30 feet or more).

If you Leap vertically, you can move up to 3 feet vertically and 5 feet horizontally onto an elevated surface.

Jumping a greater distance requires using the Athletics skill.`,
        "procedure": `If Speed >= 15 feet, move over spaces up to 10 feet.

If Speed >= 30 feet, move over spaces up to 15 feet.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": [],
        "page": 470,
    },
    {
        "name": "Ready",
        "shortDescription": "Prepare an action with a trigger",
        "description": `You prepare to use an action that will occur outside your turn. Choose a single action or free action you can use, and designate a trigger. Your turn then ends. If the trigger you designated occurs before the start of your next turn, you can use the chosen action as a reaction (provided you still meet the requirements to use it). You can’t Ready a free action that already has a trigger.

If you have a multiple attack penalty and your readied action is an attack action, your readied attack takes the multiple attack penalty you had at the time you used Ready. This is one of the few times the multiple attack penalty applies when it’s not your turn.`,
        "procedure": `Create a reaction with a trigger and an action.

End your turn.`,
        "cost": ["two_actions"],
        "tags": ["concentrate"],
        "customTags": [],
        "page": 470,
    },
    {
        "name": "Release",
        "shortDescription": "Free a hand",
        "description": `You release something you’re holding in your hand or hands. This might mean dropping an item, removing one hand from your weapon while continuing to hold it in another hand, releasing a rope suspending a chandelier, or performing a similar action. Unlike most manipulate actions, Release does not trigger reactions that can be triggered by actions with the manipulate trait (such as Attack of Opportunity).

If you want to prepare to Release something outside of your turn, use the Ready activity.`,
        "cost": ["free_action"],
        "tags": ["manipulate"],
        "customTags": [],
        "page": 470,
    },
    {
        "name": "Seek (Creature)",
        "shortDescription": "Detect Hidden and Undetected creatures",
        "description": `You scan an area for signs of creatures or objects. If you’re looking for creatures, choose an area you’re scanning. If precision is necessary, the GM can have you select a 30-foot cone or a 15-foot burst within line of sight. You might take a penalty if you choose an area that’s far away.

If you’re using Seek to search for objects (including secret doors and hazards), you search up to a 10-foot square adjacent to you. The GM might determine you need to Seek as an activity, taking more actions or even minutes or hours if you’re searching a particularly cluttered area.

The GM attempts a single secret Perception check for you and compares the result to the Stealth DCs of any undetected or hidden creatures in the area or the DC to detect each object in the area (as determined by the GM or by someone Concealing the Object). A creature you detect might remain hidden, rather than becoming observed, if you’re using an imprecise sense or if an effect (such as invisibility) prevents the subject from being observed.`,
        "procedure": `*Target* An area

*Roll* Perception

*DC* Stealth

---

*Critical Success* Target becomes observed.

*Success* Undetected target becomes Hidden and Hidden target becomes Observed.`,
        "cost": ["one_action"],
        "tags": ["concentrate", "secret"],
        "customTags": ["perception", "stealth", "observed", "hidden", "undetected"],
        "page": 471,
    },
    {
        "name": "Seek (Object)",
        "shortDescription": "Detect objects",
        "description": `See Seek (Creature)`,
        "procedure": `*Target* An area

*Roll* Perception

*DC* Stealth

---

*Critical Success* You learn the object's location.

*Success* You learn the object's location or get a clue to its whereabouts.`,
        "cost": ["one_action"],
        "tags": ["concentrate", "secret"],
        "customTags": ["perception", "stealth", "observed", "hidden", "undetected"],
        "page": 471,
    },
    {
        "name": "Sense Motive",
        "shortDescription": "Detect deception",
        "description": `You try to tell whether a creature’s behavior is abnormal. Choose one creature, and assess it for odd body language, signs of nervousness, and other indicators that it might be trying to deceive someone. The GM attempts a single secret Perception check for you and compares the result to the Deception DC of the creature, the DC of a spell affecting the creature’s mental state, or another appropriate DC determined by the GM. You typically can’t try to Sense the Motive of the same creature again until the situation changes significantly.`,
        "procedure": `*Roll* Perception

*DC* Deception or the effect

---

*Critical Success* You determine the creature’s true intentions and get a solid idea of any mental magic affecting it.

*Success* You can tell whether the creature is behaving normally, but you don’t know its exact intentions or what magic might be affecting it.

*Failure* You detect what a deceptive creature wants you to believe. If they’re not being deceptive, you believe they’re behaving normally.

*Critical Failure* You get a false sense of the creature’s intentions.`,
        "cost": ["one_action"],
        "tags": ["concentrate", "secret"],
        "customTags": ["perception", "deception"],
        "page": 471,
    },
    {
        "name": "Stand",
        "shortDescription": "Remove Prone",
        "description": `You stand up from prone.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["prone"],
        "page": 471,
    },
    {
        "name": "Step",
        "shortDescription": "Move 5 feet without triggering reactions.",
        "description": `*Requirements* Your Speed is at least 10 feet.

---

You carefully move 5 feet. Unlike most types of movement, Stepping doesn’t trigger reactions, such as Attacks of Opportunity, that can be triggered by move actions or upon leaving or entering a square.

You can’t Step into difficult terrain (page 475), and you can’t Step using a Speed other than your land Speed.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": [],
        "page": 471,
    },
    {
        "name": "Stride",
        "shortDescription": "Move up to your Speed",
        "description": `You move up to your Speed (page 463).`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": [],
        "page": 471,
    },
    {
        "name": "Strike",
        "shortDescription": "Attack",
        "description": `You attack with a weapon you’re wielding or with an unarmed attack, targeting one creature within your reach (for a melee attack) or within range (for a ranged attack). Roll the attack roll for the weapon or unarmed attack you are using, and compare the result to the target creature’s AC to determine the effect. See Attack Rolls on page 446 and Damage on page 450 for details on calculating your attack and damage rolls.`,
        "procedure": `*Critical Success* Deal double damage.

*Success* Deal damage.`,
        "cost": ["one_action"],
        "tags": ["attack"],
        "customTags": [],
        "page": 471,
    },
    {
        "name": "Take Cover",
        "shortDescription": "Increase Cover",
        "description": `*Requirements* You are benefiting from cover, are near a feature that allows you to take cover, or are prone.

---

You press yourself against a wall or duck behind an obstacle to take better advantage of cover (page 477). If you would have standard cover, you instead gain greater cover, which provides a +4 circumstance bonus to AC; to Reflex saves against area effects; and to Stealth checks to Hide, Sneak, or otherwise avoid detection. Otherwise, you gain the benefits of standard cover (a +2 circumstance bonus instead). This lasts until you move from your current space, use an attack action, become unconscious, or end this effect as a free action.`,
        "procedure": `If in Cover, gain Greater Cover, else gain Cover.`,
        "cost": ["one_action"],
        "tags": [],
        "customTags": ["bonus", "ac", "reflex", "stealth"],
        "page": 471,
    },
    {
        "name": "Arrest a Fall",
        "shortDescription": "Slow fall",
        "description": `*Trigger* You fall.

*Requirements* You have a fly Speed.

---

You attempt an Acrobatics check to slow your fall. The DC is typically 15, but it might be higher due to air turbulence or other circumstances.`,
        "procedure": `*Roll* Acrobatics

*DC* 15 [+ difficulty]

---

*Success* You fall gently, taking no damage from the fall.`,
        "cost": ["reaction"],
        "tags": [],
        "customTags": ["fly"],
        "page": 472,
    },
    {
        "name": "Avert Gaze",
        "shortDescription": "Add bonus to visual Saves",
        "description": `You avert your gaze from danger. You gain a +2 circumstance bonus to saves against visual abilities that require you to look at a creature or object, such as a medusa’s petrifying gaze. Your gaze remains averted until the start of your next turn.`,
        "procedure": `*Effect* +2 circumstance bonus to visual saves.

*Duration* Start of next turn.`,
        "cost": ["one_action"],
        "tags": [],
        "customTags": ["bonus", "visual"],
        "page": 472,
    },
    {
        "name": "Burrow",
        "shortDescription": "Move Speed",
        "description": `*Requirements* You have a burrow Speed.

---

You dig your way through dirt, sand, or a similar loose material at a rate up to your burrow Speed. You can’t burrow through rock or other substances denser than dirt unless you have an ability that allows you to do so.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["burrow"],
        "page": 472,
    },
    {
        "name": "Fly",
        "shortDescription": "Move Speed",
        "description": `*Requirements* You have a fly Speed.

---

You move through the air up to your fly Speed. Moving upward (straight up or diagonally) uses the rules for moving through difficult terrain. You can move straight down 10 feet for every 5 feet of movement you spend. If you Fly to the ground, you don’t take falling damage. You can use an action to Fly 0 feet to hover in place. If you’re airborne at the end of your turn and didn’t use a Fly action this round, you fall.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["fly"],
        "page": 472,
    },
    {
        "name": "Grab an Edge",
        "shortDescription": "Stop fall",
        "description": `*Trigger* You fall from or past an edge or handhold.

*Requirements* Your hands are not tied behind your back or otherwise restrained.

---

When you fall off or past an edge or other handhold, you can try to grab it, potentially stopping your fall. You must succeed at a Reflex save, usually at the Climb DC. If you grab the edge or handhold, you can then Climb up using Athletics.`,
        "procedure": `*Roll* Reflex Save

*DC* Climb

---

*Critical Success* Use an empty hand or held object to stop your fall and take falling damage minus 30 feet.

*Success* Use at least 1 empty hand to stop your fall and take falling damage minus 20 feet.

*Critical Failure* Take 10 bludgeoning damage for each 20ft fallen.`,
        "cost": ["reaction"],
        "tags": ["manipulate"],
        "customTags": ["reflex"],
        "page": 472,
    },
    {
        "name": "Mount",
        "shortDescription": "Mount creature",
        "description": `*Requirements* You are adjacent to a creature that is at least one size larger than you and is willing to be your mount.

---

You move onto the creature and ride it. If you’re already mounted, you can instead use this action to dismount, moving off the mount into a space adjacent to it.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": [],
        "page": 472,
    },
    {
        "name": "Point Out",
        "shortDescription": "Give detection to ally",
        "description": `*Requirements* A creature is undetected by one or more of your allies but isn’t undetected by you.

---

You indicate a creature that you can see to one or more allies, gesturing in a direction and describing the distance verbally. That creature is hidden to your allies, rather than undetected (page 466). This works only for allies who can see you and are in a position where they could potentially detect the target. If your allies can’t hear or understand you, they must succeed at a Perception check against the creature’s Stealth DC or they misunderstand and believe the target is in a different location.`,
        "procedure": `*Target* 1 creature.

*Ally* 1 or more that can detect you and the target's space.

---

If the ally can't hear or understand you, they roll Point Out (Seek) for the target.`,
        "cost": ["one_action"],
        "tags": ["auditory", "manipulate", "visual"],
        "customTags": ["hidden", "undetected"],
        "page": 472,
    },
    {
        "name": "Point Out (Seek)",
        "shortDescription": "Detect pointed out creature",
        "description": `*Requirements* An ally is pointing out an undetected creature to you.

---

See Point Out.`,
        "procedure": `*Roll* Perception

*DC* Stealth

---

*Success* The creature becomes Hidden to you.

*Fail* You believe the creature is in a different space.`,
        "cost": ["free_action"],
        "tags": ["concentrate", "secret"],
        "customTags": ["perception", "stealth", "hidden", "undetected"],
        "page": 472,
    },
    {
        "name": "Raise a Shield",
        "shortDescription": "Increase AC",
        "description": `*Requirements* You are wielding a shield.

---

You position your shield to protect yourself. When you have Raised a Shield, you gain its listed circumstance bonus to AC. Your shield remains raised until the start of your next turn.`,
        "procedure": `*Effect* +Shield circumstance bonus to AC.

*Duration* Start of next turn.`,
        "cost": ["one_action"],
        "tags": [],
        "customTags": ["bonus", "ac"],
        "page": 472,
    },
    {
        "name": "Recall Knowledge",
        "shortDescription": "Learn something",
        "description": `You attempt a skill check to try to remember a bit of knowledge regarding a topic related to that skill. The GM determines the DCs for such checks and which skills apply.`,
        "procedure": `*Roll* Skill

*DC* Simple or level [+ difficulty]

---

*Critical Success* As success and extra.

*Success* You recall useful, accurate knowledge.

*Critical Failure* You recall incorrect information.`,
        "cost": ["one_action"],
        "tags": ["concentrate", "secret"],
        "customTags": ["skill"],
        "page": 239,
    },
    {
        "name": "Balance",
        "shortDescription": "Move on precarious ground",
        "description": `*Requirements* You are in a square that contains a narrow surface, uneven ground, or another similar feature.

---

You move across a narrow surface or uneven ground, attempting an Acrobatics check against its Balance DC. You are flat-footed while on a narrow surface or uneven ground.`,
        "procedure": `*Roll* Acrobatics

*DC* Balance (p240)

---

*Critical Success* Move up to your Speed.

*Success* Move up to your Speed over difficult terrain.

*Failure* Stay still, or fall and your turn ends.

*Critical Failure* Fall and your turn ends.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["acrobatics"],
        "page": 240,
    },
    {
        "name": "Tumble Through",
        "shortDescription": "Move through an enemy",
        "description": `You Stride up to your Speed. During this movement, you can try to move through the space of one enemy. Attempt an Acrobatics check against the enemy’s Reflex DC as soon as you try to enter its space. You can Tumble Through using Climb, Fly, Swim, or another action instead of Stride in the appropriate environment.`,
        "procedure": `*Roll* Acrobatics

*DC* Reflex

---

*Success* You move through the enemy’s space, treating the squares in its space as difficult terrain. If you don’t have enough Speed to move all the way through its space, you get the same effect as a failure.

*Failure* Your movement ends, and you trigger reactions as if you had moved out of the square you started in.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["acrobatics", "reflex"],
        "page": 240,
    },
    {
        "name": "Maneuver in flight",
        "shortDescription": "Do a maneuver",
        "description": `*Requirements* You have a fly Speed.

---

You try a difficult maneuver while flying. Attempt an Acrobatics check. The GM determines what maneuvers are possible, but they rarely allow you to move farther than your fly Speed. Success You succeed at the maneuver.`,
        "procedure": `*Roll* Acrobatics

*DC* Simple

---

*Failure* Something goes wrong.

*Critical Failure* Something goes really wrong.`,
        "requirements": ["Trained in Acrobatics"],
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["fly", "acrobatics"],
        "page": 240,
    },
    {
        "name": "Climb",
        "shortDescription": "Move on a steep incline",
        "description": `*Requirements* You have both hands free.

---

You move up, down, or across an incline. Unless it’s particularly easy, you must attempt an Athletics check. The GM determines the DC based on the nature of the incline and environmental circumstances. You’re flat-footed unless you have a climb Speed.`,
        "procedure": `*Roll* Athletics

*DC* Climb (p242)

---

*Critical Success* Move 5 feet plus 5 feet per 20 feet of your land Speed.

*Success* Move 5 feet or 5 feet per 20 feet of your land Speed, whichever's higher.

*Critical Failure* You fall and if you began the climb on stable ground, you become Prone.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["athletics"],
        "page": 241,
    },
    {
        "name": "Force Open",
        "shortDescription": "Open something",
        "description": `Using your body, a lever, or some other tool, you attempt to forcefully open a door, window, container or heavy gate. With a high enough result, you can even smash through walls. Without a crowbar, prying something open takes a –2 item penalty to the Athletics check to Force Open.`,
        "procedure": `*Roll*: Athletics

*DC* Simple [+ difficulty] (p242)

---

*Critical Success* The object is Open.

*Success* The object is Broken or takes damage.

*Critical Failure* The object is Jammed (–2 circumstance penalty to Force it Open).`,
        "cost": ["one_action"],
        "tags": ["attack"],
        "customTags": ["athletics", "broken"],
        "page": 242,
    },
    {
        "name": "Grapple",
        "shortDescription": "Stop a creature from moving",
        "description": `*Requirements* You have at least one free hand. Your target cannot be more than one size larger than you.

---

You attempt to grab an opponent with your free hand. Attempt an Athletics check against their Fortitude DC. You can also Grapple to keep your hold on a creature you already grabbed.`,
        "procedure": `*Roll* Athletics

*DC* Fortitude

---

*Critical Success* Target is Restrained until the end of your next turn unless you move.

*Success* Your opponent is Grabbed until the end of your next turn unless you move.

*Failure* Any Grapple conditions end.

*Critical Failure* As failure and the target can successfully grab you or make you Prone.`,
        "cost": ["one_action"],
        "tags": ["attack"],
        "customTags": ["athletics", "fortitude", "grabbed", "restrained"],
        "page": 242,
    },
    {
        "name": "High Jump",
        "shortDescription": "Jump high",
        "description": `You Stride, then make a vertical Leap and attempt a DC 30 Athletics check to increase the height of your jump. If you didn’t Stride at least 10 feet, you automatically fail your check. This DC might be increased or decreased due to the situation, as determined by the GM.`,
        "procedure": `*Roll* Athletics

*DC* 30 [+ difficulty]

---

*Critical Success* Increase the maximum vertical distance to 8 feet, or increase the maximum vertical distance to 5 feet and maximum horizontal distance to 10 feet.

*Success* Increase the maximum vertical distance to 5 feet. Failure You Leap normally.

*Critical Failure* You don’t Leap at all, and instead you fall prone in your space.`,
        "cost": ["two_actions"],
        "tags": ["move"],
        "customTags": ["athletics", "leap"],
        "page": 242,
    },
    {
        "name": "Long Jump",
        "shortDescription": "Jump far",
        "description": `You Stride, then make a horizontal Leap and attempt an Athletics check to increase the length of your jump. The DC of the Athletics check is equal to the total distance in feet you’re attempting to move during your Leap (so you’d need to succeed at a DC 20 check to Leap 20 feet). You can’t Leap farther than your Speed.

If you didn’t Stride at least 10 feet, or if you attempt to jump in a different direction than your Stride, you automatically fail your check. This DC might be increased or decreased due to the situation, as determined by the GM.`,
        "procedure": `*Roll* Athletics

*DC* Distance up to Speed

---

*Success* Increase the maximum horizontal distance you Leap to the desired distance.

*Failure* You Leap normally.

*Critical Failure* You Leap normally, but then fall and land prone.`,
        "cost": ["two_actions"],
        "tags": ["move"],
        "customTags": ["athletics", "leap"],
        "page": 242,
    },
    {
        "name": "Shove",
        "shortDescription": "Move a creature",
        "description": `*Requirements* You have at least one hand free. The target can’t be more than one size larger than you.

---

You push an opponent away from you. Attempt an Athletics check against your opponent’s Fortitude DC.`,
        "procedure": `*Roll* Athletics

*DC* Fortitude

---

*Critical Success* You move the target up to 10 feet and can Stride after it.

*Success* You move the target up to 5 feet and can Stride after it..

*Critical Failure* You become Prone.`,
        "cost": ["one_action"],
        "tags": ["attack"],
        "customTags": ["athletics", "fortitude", "stride"],
        "page": 243,
    },
    {
        "name": "Swim",
        "shortDescription": "Move through water",
        "description": `You propel yourself through water. In most calm water, you succeed at the action without needing to attempt a check. If you must breathe air and you’re submerged in water, you must hold your breath each round. If you fail to hold your breath, you begin to drown (as described on page 478). If the water you are swimming in is turbulent or otherwise dangerous, you might have to attempt an Athletics check to Swim.

If you end your turn in water and haven’t succeeded at a Swim action that turn, you sink 10 feet or get moved by the current, as determined by the GM. However, if your last action on your turn was to enter the water, you don’t sink or move with the current that turn.`,
        "procedure": `*Roll* Athletics

*DC* Fortitude

---

*Critical Success* Move 10 feet plus 5 feet per 20 feet of your land Speed.

*Success* Move 5 feet plus 5 feet per 20 feet of your land Speed.

*Critical Failure* You make no progress, and if you’re holding your breath, you lose 1 round of air.`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["athletics", "fortitude"],
        "page": 243,
    },
    {
        "name": "Trip",
        "shortDescription": "Make creature Prone",
        "description": `*Requirements* You have at least one hand free. Your target can’t be more than one size larger than you.

---

You try to knock an opponent to the ground. Attempt an Athletics check against the target’s Reflex DC.`,
        "procedure": `*Roll* Athletics

*DC* Reflex

---

*Critical Success* Target becomes Prone and takes 1d6 bludgeoning damage.

*Success* Target becomes prone.

*Critical Failure* You become Prone.`,
        "cost": ["one_action"],
        "tags": ["attack"],
        "customTags": ["athletics", "reflex", "prone"],
        "page": 243,
    },
    {
        "name": "Disarm",
        "shortDescription": "Make creature Release an object",
        "description": `*Requirements* You have at least one hand free. The target can’t be more than one size larger than you.

---

You try to knock something out of an opponent’s grasp. Attempt an Athletics check against the opponent’s Reflex DC.`,
        "procedure": `*Roll* Athletics

*DC* Reflex

---

*Critical Success* Target is disarmed of the object.

*Success* +2 circumstance bonus to Disarm and target takes -2 circumstance penalty to attacks and checks with the item until the start of their next turn..

*Critical Failure* You become Flat-Footed until the start of your next turn.`,
        "requirements": ["Trained in Athletics"],
        "cost": ["one_action"],
        "tags": ["attack"],
        "customTags": ["athletics", "reflex", "penalty"],
        "page": 243,
    },
    {
        "name": "Create a Diversion",
        "shortDescription": "Become Hidden",
        "description": `With a gesture, a trick, or some distracting words, you can create a diversion that draws creatures’ attention elsewhere. If you use a gesture or trick, this action gains the manipulate trait. If you use distracting words, it gains the auditory and linguistic traits.

Attempt a single Deception check and compare it to the Perception DCs of the creatures whose attention you’re trying to divert. Whether or not you succeed, creatures you attempt to divert gain a +4 circumstance bonus to their Perception DCs against your attempts to Create a Diversion for 1 minute.`,
        "procedure": `*Target* 1 or more creatures

*Roll* Deception

*DC* Perception

---

Targets gain +4 circumstance bonus to Perception DC against Create a Diversion from you for 1 minute.

*Success* You become Hidden until: the end of your turn; you do something other than Step, Hide, Sneak, or Strike; or you finish a Strike action.`,
        "cost": ["one_action"],
        "tags": ["mental"],
        "customTags": ["deception", "perception", "hidden", "step", "hide", "sneak", "strike"],
        "page": 245,
    },
    {
        "name": "Lie",
        "shortDescription": "Lie",
        "description": `You try to fool someone with an untruth. Doing so takes at least 1 round, or longer if the lie is elaborate. You roll a single Deception check and compare it against the Perception DC of every creature you are trying to fool. The GM might give them a circumstance bonus based on the situation and the nature of the lie you are trying to tell. Elaborate or highly unbelievable lies are much harder to get a creature to believe than simpler and more believable lies, and some lies are so big that it’s impossible to get anyone to believe them.

At the GM’s discretion, if a creature initially believes your lie, it might attempt a Perception check later to Sense Motive against your Deception DC to realize it’s a lie. This usually happens if the creature discovers enough evidence to counter your statements.`,
        "procedure": `*Roll* Deception

*DC* Perception [+ difficulty]

---

*Success* The target believes your lie.

*Failure* The target doesn’t believe your lie and gains a +4 circumstance bonus against your attempts to Lie for the duration of your conversation. The target is also more likely to be suspicious of you in the future.`,
        "cost": [],
        "tags": ["auditory", "concentrate", "linguistic", "mental", "secret"],
        "customTags": ["deception", "perception"],
        "page": 246,
    },
    {
        "name": "Feint",
        "shortDescription": "Make creature Flat-Footed to you",
        "description": `*Requirements* You are within melee reach of the opponent you attempt to Feint.

---

With a misleading flourish, you leave an opponent unprepared for your real attack. Attempt a Deception check against that opponent’s Perception DC.`,
        "procedure": `*Roll* Deception

*DC* Perception

---

*Critical Success* Target is Flat-Footed against your melée until the end of your next turn.

*Success* Target is Flat-Footed against your next melée attack in the current turn.

*Critical Failure* You are Flat-Footed to the target's melée attacks until the end of your next turn.`,
        "requirements": ["Trained in Deception"],
        "cost": ["one_action"],
        "tags": ["mental"],
        "customTags": ["flat-footed", "deception", "perception"],
        "page": 246,
    },
    {
        "name": "Request",
        "shortDescription": "Make request of an NPC",
        "description": `You can make a request of a creature that’s friendly or helpful to you. You must couch the request in terms that the target would accept given their current attitude toward you. The GM sets the DC based on the difficulty of the request. Some requests are unsavory or impossible, and even a helpful NPC would never agree to them.`,
        "procedure": `*Roll* Diplomacy

*DC* Simple [+ difficulty]

---

*Critical Success* Target agrees.

*Success* Target agrees, possibly with a catch.

*Failure* Target refuses, possibly with a compromise.

*Critical Failure* Target refuses and their attitude to you decreases by 1.`,
        "cost": ["one_action"],
        "tags": ["auditory", "concentrate", "linguistic", "mental"],
        "customTags": ["diplomacy"],
        "page": 247,
    },
    {
        "name": "Demoralize",
        "shortDescription": "Make creature Frightened",
        "description": `With a sudden shout, a well-timed taunt, or a cutting put- down, you can shake an enemy’s resolve. Choose a creature within 30 feet of you who you’re aware of. Attempt an Intimidation check against that target’s Will DC. If the target does not understand the language you are speaking, you’re not speaking a language, or they can’t hear you, you take a –4 circumstance penalty to the check. Regardless of your result, the target is temporarily immune to your attempts to Demoralize it for 10 minutes.`,
        "procedure": `*Roll* Intimidation

*DC* Will [-4 if you aren't saying something they understand]

---

*Critical Success* The target becomes Frightened 2.

*Success* The target becomes Frightened 1.`,
        "cost": ["one_action"],
        "tags": ["auditory", "concentrate", "emotion", "mental"],
        "customTags": ["intimidation", "will", "frightened"],
        "page": 240,
    },
    {
        "name": "Administer First Aid (Stabalise)",
        "shortDescription": "Remove Dying from creature",
        "description": `*Requirements* You have healer’s tools (page 290).

---

You perform first aid on an adjacent creature that is dying. Attempt a Medicine check on a creature that has 0 Hit Points and the dying condition. The DC is equal to 5 + that creature’s recovery roll DC (typically 15 + its dying value).`,
        "procedure": `*Roll* Medicine

*DC* 5 + target's recovery roll DC

---

*Success* The creature loses the dying condition (but remains unconscious).

*Critical Failure* The creature's dying value increases by 1.`,
        "cost": ["two_actions"],
        "tags": ["manipulate"],
        "customTags": ["medicine", "dying", "unconscious"],
        "page": 248,
    },
    {
        "name": "Administer First Aid (Stop Bleeding)",
        "shortDescription": "Remove persistent Bleeding damage from creature",
        "description": `*Requirements* You have healer’s tools (page 290).

---

You perform first aid on an adjacent creature that is bleeding. Attempt a Medicine check on a creature that is taking persistent bleed damage (page 452), giving them a chance to make another flat check to remove the persistent damage. The DC is usually the DC of the effect that caused the bleed.

---

*Success* The creature attempts a flat check to end the bleeding.

*Critical Failure* The creature immediately takes an amount of damage equal to its persistent bleed damage.`,
        "procedure": `*Roll* Medicine

*DC* Effect

---

*Success* The creature attempts a flat check to end the bleeding.

*Critical Failure* The creature immediately takes an amount of damage equal to its persistent bleed damage.`,
        "cost": ["two_actions"],
        "tags": ["manipulate"],
        "customTags": ["medicine", "persistant-damage", "bleed"],
        "page": 248,
    },
    {
        "name": "Treat Poison",
        "shortDescription": "Add bonus to creature poison Save",
        "description": `*Requirements* You have healer’s tools (page 290).

---

You treat a patient to prevent the spread of poison. Attempt a Medicine check against the poison’s DC. After you attempt to Treat a Poison for a creature, you can’t try again until after the next time that creature attempts a save against the poison.`,
        "procedure": `*Roll* Medicine

*DC* Poison

---

*Critical Success* Target gains +4 circumstance bonus to its next Save against the poison.

*Success* Target gains +2 circumstance bonus to its next Save against the poison.

*Critical Failure* Target gains -2 circumstance bonus to its next Save against the poison.`,
        "requirements": ["Trained in Medicine"],
        "cost": ["one_action"],
        "tags": ["manipulate"],
        "customTags": ["medicine", "poison", "save", "bonus"],
        "page": 248,
    },
    {
        "name": "Command an Animal",
        "shortDescription": "Make request of an animal",
        "description": `You issue an order to an animal. Attempt a Nature check against the animal’s Will DC. The GM might adjust the DC if the animal has a good attitude toward you, you suggest a course of action it was predisposed toward, or you offer it a treat.

You automatically fail if the animal is hostile or unfriendly to you. If the animal is helpful to you, increase your degree of success by one step. You might be able to Command an Animal more easily with a feat like Ride (page 266).

Most animals know the Leap, Seek, Stand, Stride, and Strike basic actions. If an animal knows an activity, such as a horse’s Gallop, you can Command the Animal to perform the activity, but you must spend as many actions on Command an Animal as the activity’s number of actions. You can also spend multiple actions to Command the Animal to perform that number of basic actions on its next turn; for instance, you could spend 3 actions to Command an Animal to Stride three times or to Stride twice and then Strike.`,
        "procedure": `*Roll* Nature

*DC* Will [+ difficulty]

---

*Success* The target does as you command on its next turn.

*Failure* The target does nothing.

*Critical Failure* The target does something else.`,
        "cost": ["one_action", "two_actions", "three_actions"],
        "tags": ["auditory", "concentrate"],
        "customTags": ["nature", "will"],
        "page": 249,
    },
    {
        "name": "Perform",
        "shortDescription": "Perform",
        "description": `When making a brief performance—one song, a quick dance, or a few jokes—you use the Perform action. This action is most useful when you want to prove your capability or impress someone quickly. Performing rarely has an impact on its own, but it might influence the DCs of subsequent Diplomacy checks against the observers—or even change their attitudes— if the GM sees fit.`,
        "procedure": `*Roll* Performance

*DC* Audience (p250)

---

*Critical Success* Your performance impresses the observers, and they’re likely to share stories of your ability.

*Success* You prove yourself, and observers appreciate the quality of your performance.

*Failure* Your performance falls flat.

*Critical Failure* You demonstrate only incompetence.`,
        "cost": ["one_action"],
        "tags": ["concentrate"],
        "customTags": ["performance"],
        "page": 250,
    },
    {
        "name": "Conceal an Object",
        "shortDescription": "Hide an object",
        "description": `You hide a small object on your person (such as a weapon of light Bulk). When you try to sneak a concealed object past someone who might notice it, the GM rolls your Stealth check and compares it to this passive observer’s Perception DC. Once the GM rolls your check for a concealed object, that same result is used no matter how many passive observers you try to sneak it past. If a creature is specifically searching you for an item, it can attempt a Perception check against your Stealth DC (finding the object on success).

You can also conceal an object somewhere other than your person, such as among undergrowth or in a secret compartment within a piece of furniture. In this case, characters Seeking in an area compare their Perception check results to your Stealth DC to determine whether they find the object.`,
        "procedure": `*Roll* Stealth

*DC* Perception

---

*Success* The object is Unnoticed.

*Failure* The object is Observed.`,
        "cost": ["one_action"],
        "tags": ["manipulate", "secret"],
        "customTags": ["stealth", "perception", "unnoticed", "observed"],
        "page": 251,
    },
    {
        "name": "Hide",
        "shortDescription": "Become Hidden",
        "description": `*Requirements* You are in cover, concealed, or behind something you can take cover behind.

---

You huddle behind cover or greater cover or deeper into concealment to become hidden, rather than observed. The GM rolls your Stealth check in secret and compares the result to the Perception DC of each creature you’re observed by but that you have cover or greater cover against or are concealed from. You gain the circumstance bonus from cover or greater cover to your check.`,
        "procedure": `*Roll* Stealth

*DC* Perception

---

*Success* You become Hidden until: you are no longer in cover or concealed; you do something other than Step, Hide, Sneak, or Strike; or you finish a Strike action.`,
        "cost": ["one_action"],
        "tags": ["secret"],
        "customTags": ["stealth", "perception", "hidden", "concealed", "cover", "step", "hide", "sneak", "strike"],
        "page": 251,
    },
    {
        "name": "Sneak",
        "shortDescription": "Become Undetected while moving",
        "description": `You can attempt to move to another place while becoming or staying undetected. Stride up to half your Speed. (You can use Sneak while Burrowing, Climbing, Flying, or Swimming instead of Striding if you have the corresponding movement type; you must move at half that Speed.)

If you’re undetected by a creature and it’s impossible for that creature to observe you (for a typical creature, this includes when you’re invisible, the observer is blinded, or you’re in darkness and the creature can’t see in darkness), for any critical failure you roll on a check to Sneak, you get a failure instead. You also continue to be undetected if you lose cover or greater cover against or are no longer concealed from such a creature.

At the end of your movement, the GM rolls your Stealth check in secret and compares the result to the Perception DC of each creature you were hidden from or undetected by at the start of your movement. If you have cover or greater cover from the creature throughout your Stride, you gain the +2 circumstance bonus from cover (or +4 from greater cover) to your Stealth check. Because you’re moving, the bonus increase from Taking Cover doesn’t apply. You don’t get to roll against a creature if, at the end of your movement, you neither are concealed from it nor have cover or greater cover against it. You automatically become observed by such a creature.`,
        "procedure": `*Target* 1 or more creatures you are not Observed by

*Roll* Stealth [+ minimum Cover bonus during movement]

*DC* Perception

---

Stride up to half your speed.

If you end movement neither Concealed or in Cover, Critically Fail.

*Success* You become Undetected until: You do something other than Step, Hide, Sneak, or Strike; or you finish a Strike action.

*Failure* You become Hidden.

*Critical Failure* If you are not Invisible, you become Observed. Else you become Hidden`,
        "cost": ["one_action"],
        "tags": ["move"],
        "customTags": ["stealth", "perception", "cover", "concealed", "observed", "hidden", "undetected", "invisible"],
        "page": 252,
    },
    {
        "name": "Palm an Object",
        "shortDescription": "Take an unattended object",
        "description": `Palming a small, unattended object without being noticed requires you to roll a single Thievery check against the Perception DCs of all creatures who are currently observing you. You take the object whether or not you successfully conceal that you did so. You can typically only Palm Objects of negligible Bulk, though the GM might determine otherwise depending on the situation.`,
        "procedure": `*Roll* Thievery

*DC* Perception

---

*Success* You take the object without being noticed.

*Failure* You take the object and get noticed.`,
        "cost": ["one_action"],
        "tags": ["manipulate"],
        "customTags": ["thievery", "perception"],
        "page": 253,
    },
    {
        "name": "Steal",
        "shortDescription": "Take an attended object",
        "description": `You try to take a small object from another creature without being noticed. Typically, you can Steal only an object of negligible Bulk, and you automatically fail if the creature who has the object is in combat or on guard.

Attempt a Thievery check to determine if you successfully Steal the object. The DC to Steal is usually the Perception DC of the creature wearing the object. This assumes the object is worn but not closely guarded (like a loosely carried pouch filled with coins, or an object within such a pouch). If the object is in a pocket or similarly protected, you take a –5 penalty to your Thievery check. The GM might increase the DC of your check if the nature of the object makes it harder to steal (such as a very small item in a large pack, or a sheet of parchment mixed in with other documents).

You might also need to compare your Thievery check result against the Perception DCs of observers other than the person wearing the object. The GM may increase the Perception DCs of these observers if they’re distracted.`,
        "procedure": `*Roll* Thievery

*DC* Perception (+difficulty)

---

*Success* You take the object without being noticed.

*Failure* You don't take the object and the bearer notices.`,
        "cost": ["one_action"],
        "tags": ["manipulate"],
        "customTags": ["thievery", "perception"],
        "page": 253,
    },
    {
        "name": "Disable a Device",
        "shortDescription": "Disable a Device",
        "description": `*Requirements* Some devices require you to use thieves’ tools (page 291) when disabling them.

---

This action allows you to disarm a trap or another complex device. Often, a device requires numerous successes before becoming disabled, depending on its construction and complexity. Thieves’ tools are helpful and sometimes even required to Disable a Device, as determined by the GM, and sometimes a device requires a higher proficiency rank in Thievery to disable it.

Your Thievery check result determines how much progress you make.`,
        "procedure": `*Roll* Thievery

*DC* Device

---

*Critical Success* You gain 2 successes, leave no evidence, and can rearm the device.

*Success* You gain 1 success.

*Critical Failure* You trigger the device.`,
        "requirements": ["Trained in Thievery"],
        "cost": ["two_actions"],
        "tags": ["manipulate"],
        "customTags": ["thievery"],
        "page": 253,
    },
    {
        "name": "Pick a Lock",
        "shortDescription": "Open a lock",
        "description": `*Requirements* You have thieves’ tools (page 291).

---

Opening a lock without a key is very similar to Disabling a Device, but the DC of the check is determined by the complexity and construction of the lock you are attempting to pick (locks and their DCs are found on page 290). Locks of higher qualities might require multiple successes to unlock, since otherwise even an unskilled burglar could easily crack the lock by attempting the check until they rolled a natural 20. If you lack the proper tools, the GM might let you used improvised picks, which are treated as shoddy tools, depending on the specifics of the lock.`,
        "procedure": `*Roll* Thievery

*DC* Device

---

*Critical Success* You gain 2 successes and leave no evidence.

*Success* You gain 1 success.

*Critical Failure* You break your tools.`,
        "cost": ["two_actions"],
        "tags": ["manipulate"],
        "customTags": ["thievery"],
        "page": 253,
    },
];

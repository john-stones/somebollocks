# Contributing

## Commit Message Guidelines

This project adheres to [Conventional Commits][conventional-commits] with
[Angular commit types][angular-commit-types].

[angular-commit-types]: https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#type
[conventional-commits]: https://www.conventionalcommits.org/en/v1.0.0/

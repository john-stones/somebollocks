# Some Bollocks

[![standard-readme compliant][standard-readme-badge]][standard-readme]

[![mit licence][licence-mit-badge]][licence-mit]

The code for [Some Bollocks](http://www.somebollocksthatidone.com/).

## Usage

```
docker run --publish 80:80 registry.gitlab.com/john.stones/somebollocks
```

## Contributing

PRs accepted.

SEE CONTRIBUTING in [CONTRIBUTING.md](CONTRIBUTING.md)

## License

[MIT][licence-mit] © John M. Stones

SEE LICENSE IN [LICENSE](LICENSE)

[standard-readme]: https://github.com/RichardLitt/standard-readme
[standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-informational.svg
[licence-mit]: https://spdx.org/licenses/MIT.html
[licence-mit-badge]: https://img.shields.io/badge/license-MIT-informational.svg
